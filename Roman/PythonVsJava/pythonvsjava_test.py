from unittest import TestCase
from pythonvsjava import RomanNumber

class Test(TestCase):

    def test_error_input(self):
        test = RomanNumber()
        self.assertEqual("Give us a number between 1 and 3999", test.parse_to_roman(0))
        self.assertEqual("Give us a number between 1 and 3999", test.parse_to_roman(4222))
        self.assertEqual("That is not a number", test.parse_to_roman("42&2"))

    def test_arabic_to_roman_count_for_ten_power(self):
        test = RomanNumber()
        self.assertEqual({1000: 0, 100: 0, 10: 0, 1: 1}, test.count_for_10_power(1))
        self.assertEqual({1000: 0, 100: 0, 10: 2, 1: 0}, test.count_for_10_power(20))
        self.assertEqual({1000: 0, 100: 0, 10: 5, 1: 0}, test.count_for_10_power(50))
        self.assertEqual({1000: 0, 100: 1, 10: 1, 1: 6}, test.count_for_10_power(116))
        self.assertEqual({1000: 1, 100: 2, 10: 8, 1: 8}, test.count_for_10_power(1288))

    def test_arabic_to_roman_one_character(self):
        test = RomanNumber()
        self.assertEqual("I", test.parse_to_roman(1))
        self.assertEqual("X", test.parse_to_roman(10))
        self.assertEqual("C", test.parse_to_roman(100))
        self.assertEqual("M", test.parse_to_roman(1000))

    def test_arabic_to_roman_all_but_4_exception(self):
        test = RomanNumber()
        self.assertEqual("III", test.parse_to_roman(3))
        self.assertEqual("XX", test.parse_to_roman(20))
        self.assertEqual("CCCXII", test.parse_to_roman(312))
        self.assertEqual("MMMCCI", test.parse_to_roman(3201))

    def test_arabic_to_roman_with_4_exception(self):
        test = RomanNumber()
        self.assertEqual("XL", test.parse_to_roman(40))
        self.assertEqual("CMXC", test.parse_to_roman(990))
        self.assertEqual("MMCMXLII", test.parse_to_roman(2942))
        self.assertEqual("MMMCMXCIX", test.parse_to_roman(3999))

    def test_roman_to_arabic_one_character(self):
        test = RomanNumber()
        self.assertEqual(5, test.parse_to_arabic("V"))
        self.assertEqual(10, test.parse_to_arabic("X"))
        self.assertEqual(100, test.parse_to_arabic("C"))

    def test_roman_to_arabic_several_characters(self):
        test = RomanNumber()
        self.assertEqual(3999, test.parse_to_arabic("MMMCMXCIX"))
        self.assertEqual(2942, test.parse_to_arabic("MMCMXLII"))
        self.assertEqual(1970, test.parse_to_arabic("MCMLXX"))

    def test_check_roman_number(self):
        test = RomanNumber()
        self.assertEqual("Erreur de saisie du nombre romain", test.parse_to_arabic("XML"))
        self.assertEqual("Erreur de saisie du nombre romain", test.parse_to_arabic("CCCC"))
        self.assertEqual("Erreur de saisie du nombre romain", test.parse_to_arabic("R"))
        self.assertEqual("Erreur de saisie du nombre romain", test.parse_to_arabic("CLL"))
        self.assertEqual("Erreur de saisie du nombre romain", test.parse_to_arabic("LC"))
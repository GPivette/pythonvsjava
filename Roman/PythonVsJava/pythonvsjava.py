from itertools import groupby


class RomanNumber:

    def __init__(self):
        self.roman_to_arab = {'M': 1000,
                              'D': 500,
                              'C': 100,
                              'L': 50,
                              'X': 10,
                              'V': 5,
                              'I': 1}

        self.arab_to_roman = {1000: 'M',
                              500: 'D',
                              100: 'C',
                              50: 'L',
                              10: 'X',
                              5: 'V',
                              1: 'I'}

    @staticmethod
    def count_for_10_power(number):
        m = int(number / 1000)
        number -= m * 1000
        c = int(number / 100)
        number -= c * 100
        x = int(number / 10)
        number -= x * 10

        return {1000: m, 100: c, 10: x, 1: number}

    def parse_to_roman(self, number):
        roman_number = []

        try: 
            number = int(number)
        except ValueError:
            return "That is not a number"

        if number < 1 or number > 3999:
            return "Give us a number between 1 and 3999"

        powers_of_10 = RomanNumber.count_for_10_power(number)

        for key, value in powers_of_10.items():
            if value > 0:
                if value not in [4, 9]:
                    roman_number.append(self.arab_to_roman[key] * value)
                else:
                    roman_number.append(self.arab_to_roman[key])
                    roman_number.append(self.arab_to_roman[key * (value + 1)])

        return ''.join(roman_number)

    def check_roman_number(self, number):
        blocks = [''.join(g) for k, g in groupby(number)]
        for block in blocks:
            if len(block) > 3:
                return False
        for char in number:
            if not char in self.roman_to_arab.keys():
                return False
        for i in range(len(number) - 1):
            if number[i] == number[i + 1] and number[i] in ['D', 'L', 'V']:
                return False
            if self.roman_to_arab[number[i]] < self.roman_to_arab[number[i + 1]]:
                if number[i] in ['D', 'L', 'V']:
                    return False
                elif self.roman_to_arab[number[i]] * 10 != self.roman_to_arab[number[i + 1]] and self.roman_to_arab[number[i]] * 5 != self.roman_to_arab[number[i + 1]]:
                    return False
        return True

    def parse_to_arabic(self, number):
        arabic_number = 0

        if not self.check_roman_number(number):
            return "Erreur de saisie du nombre romain"

        else:
            print('OK')
            len_number = len(number)
            for i in range(len(number) - 1):
                if self.roman_to_arab[number[i]] < self.roman_to_arab[number[i + 1]]:
                    arabic_number -= self.roman_to_arab[number[i]]
                else:
                    arabic_number += self.roman_to_arab[number[i]]
            arabic_number += self.roman_to_arab[number[len_number - 1]]

        return arabic_number